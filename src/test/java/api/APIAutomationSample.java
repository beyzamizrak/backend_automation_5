package api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class APIAutomationSample {
    public static void main(String[] args) {

        /**
         * response is an interface coming from the RestAssured library
         * The Response variable "response" stores all the components of API calls including the request abd response
         * RestAssured is written with BDD flow
         *
         */

        Response response;

        Faker faker = new Faker();


        System.out.println("\n--------------------POST----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 576e228b2474025106285e98b51fccc6d540fe4feceea81ba30d553ba372f1c1")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() + "\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress()+ "\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().post("https://gorest.co.in/public/v2/users")
                .then().log().all().extract().response();

        System.out.println(response.asString());


        System.out.println("\n--------------------GET SPECIFIC----------------------------------\n");


        int postId = response.jsonPath().getInt("id");

        System.out.println("Id is coming from response " + postId);

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 576e228b2474025106285e98b51fccc6d540fe4feceea81ba30d553ba372f1c1")
                .when().get("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

        System.out.println("\n----------------------GET ALL-------------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 576e228b2474025106285e98b51fccc6d540fe4feceea81ba30d553ba372f1c1")
                .when().get("https://gorest.co.in/public/v2/users")
                .then().log().all().extract().response();

        System.out.println("\n------------------------PUT-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 576e228b2474025106285e98b51fccc6d540fe4feceea81ba30d553ba372f1c1")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() + "\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress()+ "\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().put("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

        System.out.println("\n------------------------PATCH-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 576e228b2474025106285e98b51fccc6d540fe4feceea81ba30d553ba372f1c1")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() + "\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress()+ "\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().patch("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

        int patchId = response.jsonPath().getInt("id");

        Assert.assertEquals(postId, patchId, "Expected id " + postId + " we found " + patchId);

        System.out.println("\n------------------------DELETE-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 576e228b2474025106285e98b51fccc6d540fe4feceea81ba30d553ba372f1c1")
                .when().delete("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

    }
}
