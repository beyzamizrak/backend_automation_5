package api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.Objects;

public class TGAPIAutomation {

    public static void main(String[] args) {

        Response response;

        Faker faker = new Faker();

        System.out.println("\n--------------------TG-POST----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"" + faker.date().birthday() + "\n" +
                        "}")
                .when().post("https://tech-global-training.com/students")
                .then().log().all().extract().response();

        System.out.println(response.asString());

        System.out.println("\n--------------------TG-GET-SPECIFIC----------------------------------\n");

        int TG_Id = response.jsonPath().getInt("id");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().get("https://tech-global-training.com/students/" + TG_Id)
                .then().log().all().extract().response();

        System.out.println(response.asString());

        System.out.println("\n--------------------TG-GET-ALL----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().get("https://tech-global-training.com/students")
                .then().log().all().extract().response();

        System.out.println(response.asString());

        System.out.println("\n------------------------TG-PUT-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"" + faker.date().birthday() + "\n" +
                        "}")
                .when().put("https://tech-global-training.com/students/" + TG_Id)
                .then().log().all().extract().response();

        System.out.println(response.asString());

        System.out.println("\n------------------------TG-PATCH-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"" + faker.date().birthday() + "\n" +
                        "}")
                .when().patch("https://tech-global-training.com/students/" + TG_Id)
                .then().log().all().extract().response();

        System.out.println(response.asString());

        System.out.println("\n------------------------TG-DELETE-SPECIFIC-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().delete("https://tech-global-training.com/students/" + TG_Id)
                .then().log().all().extract().response();

        System.out.println(response.asString());


        System.out.println("\n------------------------TG-DELETE-ALL-----------------------------------\n");

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().delete("https://tech-global-training.com/students/deleteall")
                .then().log().all().extract().response();

        System.out.println(response.asString());





    }
}