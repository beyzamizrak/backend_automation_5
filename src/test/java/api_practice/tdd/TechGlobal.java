package api_practice.tdd;

import api_practice.pojo_classes.tech_global.CreateTGUser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import sun.java2d.pipe.SpanShapeRenderer;
import utils.ConfigReader;

import java.text.SimpleDateFormat;
import java.time.LocalDate;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsEqual.equalToObject;

public class TechGlobal {

    Response response;

    ObjectMapper objectMapper = new ObjectMapper();

    Faker faker = new Faker();

    String expectedFirstName;
    String expectedLastName;
    String expectedEmail;
    String expectedDOB;



    @BeforeTest
    public void beforeTest() {
        System.out.println("Starting the API test");
        // By having RestAssured URI set implicitly in to rest assured
        // we just add path to the post call
        RestAssured.baseURI = ConfigReader.getProperty("tgURI");
    }

    @Test
    public void createTGUser() throws JsonProcessingException {

        CreateTGUser createTGUser = new CreateTGUser();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // create formatter
        String dob = sdf.format(faker.date().birthday()); // generate a format birthday

        createTGUser.setFirstName(faker.name().firstName());
        createTGUser.setLastName(faker.name().lastName());
        createTGUser.setEmail(faker.internet().emailAddress());
        createTGUser.setDob(dob);


        expectedFirstName = createTGUser.getFirstName();
        expectedLastName = createTGUser.getLastName();
        expectedEmail = createTGUser.getEmail();
        expectedDOB = createTGUser.getDob();


        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .body(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(createTGUser))
                .when().post("/students")
                .then().log().all()
                .and().assertThat().statusCode(200)
                .and().body("firstName", equalTo(expectedFirstName))
                .and().body("email", equalTo(expectedEmail))
                .time(Matchers.lessThan(2000L))
                .extract().response();






    }

}
